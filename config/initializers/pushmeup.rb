if Rails.env.production?
  NotificationService::PushNotification.load_ios_configuration
  NotificationService::PushNotification.load_android_configuration
end
