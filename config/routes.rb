Rails.application.routes.draw do
  resources :devices#, only: [:index, :create]
  resources :measurements
  resources :movements do
  	delete :delete_all, on: :collection
  end
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
