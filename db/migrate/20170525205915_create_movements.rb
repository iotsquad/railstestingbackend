class CreateMovements < ActiveRecord::Migration[5.1]
  def change
    # drop_table :movements
    create_table :movements do |t|
      t.string :value
      t.string :temperature
      t.string :humidity

      t.timestamps
    end
  end
end
