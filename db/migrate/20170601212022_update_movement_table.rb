class UpdateMovementTable < ActiveRecord::Migration[5.1]
  def change
  	remove_column :movements, :temperature
  	remove_column :movements, :humidity
  	add_column :movements, :sensor, :string
  end
end
