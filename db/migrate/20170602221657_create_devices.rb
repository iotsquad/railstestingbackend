class CreateDevices < ActiveRecord::Migration[5.1]
  def change
    create_table :devices do |t|
      t.string :uuid
      t.string :token
      t.string :platform

      t.timestamps
    end
  end
end
