class CreateMeasurements < ActiveRecord::Migration[5.1]
  def change
    create_table :measurements do |t|
      t.string :temperature
      t.string :humidity
      t.string :sensor

      t.timestamps
    end
  end
end
