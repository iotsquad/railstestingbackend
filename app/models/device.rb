class Device < ApplicationRecord
  validates :uuid, :platform, presence: true

  enum platform: { ios: "ios", android: "android" }

end
