class Movement < ApplicationRecord
  after_create :notify

  def notify
    data = {
      title: 'Movement detected',
      subtitle: 'Please review this alert ASAP!',
      movement: {
        sensor: self.sensor,
        created_at: self.created_at.to_s
      }
    }
    Device.all.each do |device|
      if device.ios?
        payload = {
          :other => data,
          :"content-available" => 1
        }
        NotificationService::PushNotification.send(:ios, device.token, payload)
      elsif device.android?
        NotificationService::PushNotification.send(:android, device.token, data)
      end
    end
  end
end
