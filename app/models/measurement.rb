class Measurement < ApplicationRecord
  after_create :send_to_firebase

  def send_to_firebase
    base_uri = ENV['FIREBASE_BASE_URI'] || 'https://housealert-daf2e.firebaseio.com/'
    secret_key = ENV['FIREBASE_DB_SECRET_KEY'] || 'UQr4hWos3F1nqim2IIxzO7LRIDumo4rFcXp1urib'

    firebase = Firebase::Client.new(base_uri, secret_key)

    firebase.set('last_humidity_percentage', self.humidity.to_f)
    firebase.set('last_temperature_celcius', self.temperature.to_f)

    today_averages_hash = today_averages
    firebase.set('today_humidity_percentage', today_averages_hash[:humidity_percentage_averages])
    firebase.set('today_temperature_celcius', today_averages_hash[:temperature_averages])
  end

  def today_averages
    humidity_percentage_averages = []
    temperature_averages = []
    measurements = []
    time_now = Time.zone.now
    (time_now.beginning_of_day.to_i .. time_now.to_i).step(1.hour) do |date|
      time = Time.at(date)
      if time <= time_now
        measurements = Measurement.where('? <= created_at AND created_at < ?', time, time + 1.hour)
        temperatures = measurements.map(&:temperature).map(&:to_f)
        humidity_percentages = measurements.map(&:humidity).map(&:to_f)
        temperature_averages << (temperatures.empty? ? 0 : (temperatures.inject(:+) / temperatures.size))
        humidity_percentage_averages << (humidity_percentages.empty? ? 0 : (humidity_percentages.inject(:+) / humidity_percentages.size))
      end
    end
    return { humidity_percentage_averages: humidity_percentage_averages, temperature_averages: temperature_averages }
  end

end
