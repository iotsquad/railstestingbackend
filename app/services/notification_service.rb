module NotificationService

  class PushNotification
    def self.load_ios_configuration
      APNS.host = ENV['APNS_HOST']
      APNS.port = ENV['APNS_PORT']
      APNS.pem = File.join(Rails.root, 'config', 'certificates', ENV['APNS_CERTIFICATE_NAME'].to_s)
      APNS.pass = ENV['APNS_CERTIFICATE_PASS']
    end

    def self.load_android_configuration
      FCM.new(ENV['FCM_KEY'])
    end

    def self.send(platform, key, payload)
      begin
        case platform
        when :ios
          APNS.send_notification(key, payload)
        when :android
          fcmsn = FCM.new(ENV['FCM_KEY'])
          fcmsn.send([key], { data: payload })
        end
      rescue Exception => e
        Rails.logger.error "error: #{e}"
      end
    end
  end

end
