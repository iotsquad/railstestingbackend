json.extract! movement, :id, :value, :sensor, :created_at, :updated_at
json.url movement_url(movement, format: :json)
